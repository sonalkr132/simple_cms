Rails.application.routes.draw do
  root :to => "public#index"
  resources :admin_user
  
  get 'demo/index'
  resources :subjects
  get 'subjects/:id/delete' => 'subjects#delete'

  resources :pages
  get 'pages/:id/delete' => 'pages#delete'

  resources :sections
  get 'sections/:id/delete' => 'sections#delete'

  get 'demo/javascript'

  get 'admin' => 'access#menu'
  get 'access/logout' => 'access#logout'
  post 'access/attempt_login' => 'access#attempt_login'
  get 'access/login' => 'access#login'
  get 'access' => 'access#index'
  get 'access/index' => 'access#index'

  resources :admin_user
  get 'admin_user/:id/delete' => 'admin_user#delete'

  get 'show/:id' => 'public#show'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
